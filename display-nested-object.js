const obj = {
  "username": "reilg",
  "password": "password",
  "database": "sql_dev",
  "host": "127.0.0.1",
  "dialect": "mysql",
  "define": {
    "underscored": true
  },
  some: {
    other: {
      nested: {
        value: 'value here',
      }
    }
  }
}

function display(obj, propStr = '') {
  Object.entries(obj).forEach(([key, val]) => {
    const nestedPropStr = propStr + (propStr ? '.' : '') + key;
    if (typeof val === 'object') display(val, nestedPropStr);
    else console.log(nestedPropStr + ':', val);
  });
}

console.clear()
display(obj)
