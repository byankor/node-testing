const user = {
  id: 1,
  first_name: "Han",
  last_name: "Solo",
  ignore_me: "Please!",
};

function serial() {
  const ser = require("./serial");

  const data = {
    id: 1,
    first_name: "Han",
    last_name: "Solo",
    ignore_me: "Please!",
  };

  const IN = ser.serialize("user", data, { count: 1 });
  console.log("IN:", IN);

  const OUT = ser.deserialize("user", IN);
  console.log("OUT:", OUT);
}

function cls() {
  const { User } = require("./models");
  console.log(new User());
}

function itr() {
  Object.entries(user).forEach((entry) => {
    console.log("E:", entry);
    const [key, value] = entry;
    console.log("KV", key, value);
  });
}

const fs = require("fs");
const axios = require("axios");
const FormData = require("form-data");

function str() {
  const fd = new FormData();
  // fd.append('file', 'ip address')
  // fd.append('file', 'date signed')
  // fd.append('file', 'email address')

  const data = `ip address\ndate signed\nemail address`;
  fd.append("file", data);

  axios.post("https://postman-echo.com/post", fd).then((response) => {
    console.log(response);
  });
}

function prms() {
  const pr1 = new Promise((res, rej) => {
    res();
  });
  pr1
    .then(async () => {
      try {
        const err = await new Error("shit");
        throw err;
      } catch (e) {
        console.log("oi");
        return Promise.reject(e);
      }
    })
    .catch((e) => {
      console.log("huh", e);
    });
}

serial();
