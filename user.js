class User {
  constructor(firstName, lastName) {
    this.firstName = firstName || "Han"
    this.lastName = lastName || "Solo"
  }

  fullName() {
    this.firstName + " " + this.lastName
  }
}

module.exports = User
