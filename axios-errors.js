const axios = require('axios')

async function get() {
  try {
    await axios.get('https://httpbin.org/status/500')
  } catch (err) {
    console.log(err.toJSON())
    if (err.response) {
      console.log("RES", err.response)
    } else if (err.request) {
      console.log("REQ", err.request)
    } else {
      console.log("ERR", err)
    }
  }
}

console.clear()
get()
