/**
 * Get Session data from V2 API
 * 1. Run v2 API
 * 2. node $filename
 */
const axios = require('axios')
const { deserialize } = require('deserialize-json-api')

async function main() {
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjo1MSwiaWF0IjoxNjIzNjcxMTg0LCJleHAiOjE2MjUzODEyNTU4MDR9.p4hoeMpV6AaAVUD5ci_G8yXj-ecouFA3yCcsqvmHqQY'
  const options = {
    method: 'GET',
    url: 'http://api.payrent.local:3010/v2/session',
    params: {'page\[size\]': '1', include: 'paymentSettings,property'},
    headers: {
      Connection: 'keep-alive',
      Accept: 'application/vnd.api+json',
      Authorization: `Bearer ${token}`
    }
  };

  try {
    const response = await axios.request(options)
    const out = deserialize(response.data);
    console.log(out)
  } catch (err) {
    console.log(JSON.stringify(err))
  }

}

console.clear()
main()
