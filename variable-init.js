const test = {
  isNull: null,
  hasValue: "TEST",
}

const shouldBeNull = test && test.isNull
const hasValue = test && test.hasValue || "WRONG"
const isInit = null && test.hasValue || "INIT"

const t2 = undefined
const isInit2 = t2 && t2.value || "INIT 2"
console.clear()
console.log("SHOULD BE NULL:", shouldBeNull)
console.log("SHOULD BE TEST:", hasValue)
console.log("SHOULD BE INIT:", isInit)
console.log("SHOULD BE INIT 2:", isInit2)

