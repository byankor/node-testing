module.exports.log = console.log
module.exports.clear = console.clear
module.exports.sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}
