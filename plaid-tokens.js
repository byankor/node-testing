const log = console.log
const clear = console.clear

const { Configuration, PlaidApi, PlaidEnvironments } = require('plaid')

const CLIENT_ID='redacted'
const SECRET='redacted'
const API='https://sandbox.plaid.com'

const configuration = new Configuration({
  basePath: PlaidEnvironments.sandbox,
  baseOptions: {
    headers: {
      'PLAID-CLIENT-ID': CLIENT_ID,
      'PLAID-SECRET': SECRET,
    }
  }
})

const client = new PlaidApi(configuration)

async function getInstitutions() {
  // Pull institutions
  const request = {
    count: 10,
    offset: 0,
    country_codes: ['US'],
  };
  try {
    const response = await client.institutionsGet(request);
    const institutions = response.data.institutions;
    console.log('in', institutions);
  } catch (error) {
    throw Error(error)
  }
}

async function searchInstitutions(term) {
  const request = {
    query: term,
    products: ['transactions'],
    country_codes: ['US'],
  };
  try {
    const response = await client.institutionsSearch(request);
    const institutions = response.data.institutions;
    console.log(institutions)
  } catch (error) {
    throw Error(error)
  }
}

async function getPublicToken() {
  const publicTokenRequest = {
    institution_id: 'ins_5', // Citibank online
    initial_products: ['auth', 'assets', 'balance'],
 };
  try {
    const publicTokenResponse = await client.sandboxPublicTokenCreate(publicTokenRequest);
    const publicToken = publicTokenResponse.data.public_token;

    return publicToken;
  } catch (error) {
    throw Error(error);
  }
}

async function getAccessToken(token) {
  // The generated public_token can now be exchanged
  // for an access_token
  const exchangeRequest = { public_token: token };
  const exchangeTokenResponse = await client.itemPublicTokenExchange(exchangeRequest);
  const accessToken = exchangeTokenResponse.data.access_token;
  return accessToken;
}

async function getProcessorToken(accessToken) {
  try {

    accessToken = 'access-sandbox-bacb963b-a8f3-484c-93a9-3a8cef2979f5'
    // Create a processor token for a specific account id.
    const request = {
      access_token: accessToken,
      account_id: "Nz7y9KB5dziorzD8vqNNFgzBLqdnGZHWoPJQ3",
      processor: 'sila_money',
    };
    const processorTokenResponse = await client.processorTokenCreate(request);
    log('r', processorTokenResponse)
    return processorTokenResponse.data.processor_token;
  } catch (error) {
    log(error.response.data)
    if (error.response)
      throw Error(error.response.data);
  }
}

async function linkAccount(token) {
  const handle = 'hoban_washburne_01'
  const walletKey = '0xbb649a6942264a05eddc66c64df2dff0737d29f38b109f0c86a6fc9de7e00d6b'

  const res = await api.linkAccount(
    handle,
    walletKey,
    token,
    metadata.account.name,
    metadata.account_id,
    'link'
  );

  return res
}

async function run() {
  try {
    clear()
    // let pubtoken = await getPublicToken()
    // log('t', pubtoken)

    // let atoken = await getAccessToken(pubtoken)
    // log('a', atoken)

    let ptoken = await getProcessorToken('access-sandbox-bacb963b-a8f3-484c-93a9-3a8cef2979f5')
    log('p', ptoken)
  } catch(error) {
    log('Err', error)
  }
}

run()
