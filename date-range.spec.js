const { getRange } = require('./date-range.js')
const moment = require('moment')

const fmt = 'YYYY-MM-DD'

describe('Testing getRange()', () => {
  const tests = [
    ['2021-01-01', 1, 3, '2021-01-05', '2021-02-04'],
    ['2021-01-01', 5, 3, '2020-12-09', '2021-01-08'],
    ['2021-01-15', 5, 3, '2021-01-09', '2021-02-08'],
    ['2021-02-15', 31, 3, '2021-02-04', '2021-03-03'],
    ['2021-02-28', 31, 3, '2021-02-04', '2021-03-03'],
  ]

  it.each(tests)('mock %s, select %i', async (mockdate, selectedDay, grace, prev, next) => {
    jest.spyOn(Date, 'now').mockImplementation(() => new Date(mockdate))

    const expPrev = moment(prev).startOf('day').format()
    const expNext = moment(next).endOf('day').format()
    log("P", expPrev, "N", expNext)

    const { prev: p, next: n } = getRange(selectedDay, grace)
    expect(p.format()).toBe(expPrev)
    expect(n.format()).toBe(expNext)
  })
})
