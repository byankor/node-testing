class Base {
  #date

  constructor(data) {
    Object.assign(this, data)
  }

  get date() {
    if (!this.#date) {
      return 'today'
    }

    return this.#date
  }

  set date(date) {
    this.#date = date
  }

  toJSON() {
    return JSON.stringify(this.toObject(), null, 2)
  }
}

class Child extends Base {
  customID = 123
  constructor(data) {
    super()
    Object.assign(this, data)
  }

  toObject() {
    return {
      id: this.customID,
      name: this.name,
      date: this.date,
    }
  }
}

module.exports = {
  Child,
  Base,
}
