async function a() {
  console.clear()
  try {
    await b()
  } catch(err) {
    throw err
  }
}

async function b() {
  await c()
}

async function c() {
  if (true) {
    throw new Error('C ERR')
  }
}

(async () => {
  try {
    await a()
  } catch(err) {
    throw err
  }
})().catch(err => console.log(err))
