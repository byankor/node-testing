const { log, clear, sleep } = require('./helpers')

async function test() {
  await sleep(1000)
  return new Promise(r => r("Good"))
}

async function f1() {
  return test()
}

/**
 * It is good practice to use return await fn() in try..catch blocks
 * because only return fn() inside try..catch does not catch the error
 * from fn()
 */
async function run() {
  try {
    const o1 = await f1()
    return await f1() // can catch error

    return f1() // will immediately return the promise
                // and not wait for exception

  } catch (err) {
    log('error', err)
  }
}

run()
