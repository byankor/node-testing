const moment = require('moment')

module.exports.getRange = (selectedDay, grace) => {
  const today = moment()

  let prev, next
  if (today.date() >= selectedDay) {
    prev = today.clone().startOf('month')
    next = today.clone().add(1, 'months').startOf('month')
  }
  else {
    prev = today.clone().subtract(1, 'months').startOf('month')
    next = today.clone()
  }

  prev = this.limiter(prev, selectedDay)
  next = this.limiter(next, selectedDay)

  prev.startOf('day').add(grace+1, 'day')
  next.endOf('day').add(grace, 'day')

  return { prev, next }
}

module.exports.limiter = (dateToLimit, selectedDay) => {
  if (moment(dateToLimit).endOf('month').isBefore(moment(dateToLimit).date(selectedDay)))
    dateToLimit = moment(dateToLimit).endOf('month');
  else
    dateToLimit = moment(dateToLimit).date(selectedDay);

  return dateToLimit;
}

// To force a date I inserted a initDate like parameters, remove it if it's unnecessary
module.exports._getRange = (selectedDay, initDate) => {
  const today = moment(initDate);

  let previous;
  let next;

  if (today.date() >= selectedDay) {
    previous = moment(today).startOf('month');
    next = moment(today).add(1, 'month').startOf('month');
  } else {
    previous = moment(today).subtract(1, 'months');
    next = moment(today);
  }

  previous = limiter(previous, selectedDay); // this function get the max date of the month
  next = limiter(next, selectedDay);

  return [previous, next];
}
