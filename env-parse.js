/**
 * Parse env files and show a comparison table
 */
const dotenv = require('dotenv')
const chalk = require('chalk')
const chalkTable = require('chalk-table')

const f1 = '.env.stage'
const f2 = '.env.stage.current'

const e1 = dotenv.parse(require('fs').readFileSync(f1))
const e2 = dotenv.parse(require('fs').readFileSync(f2))
const keys = Object.keys({ ...e1, ...e2 })

const options = {
  leftPad: 2,
  columns: [
    { field: 'key', name: 'Key' },
    { field: 'e1', name: chalk.magenta(`E1 -> ${f1}`) },
    { field: 'e2', name: chalk.cyan(`E2 -> ${f2}`) },
    { field: 'diff', name: chalk.green('Diff') },
  ],
}

const warning = chalk.hex('#FFA500'); // Orange color
const missingString = chalk.bold.red('--')

const getDiff = (a = null, b = null) => {
  let msg = ''

  if (a && !b) {
    msg = warning(`missing on ${chalk.bold.cyan(f2)}`)
  } else if (!a && b) {
    msg = warning(`missing on ${chalk.bold.magenta(f1)}`)
  }

  if (a && b && a !== b) {
    msg = warning(`values mismatch`)
  }

  return msg
}

const table = chalkTable(
  options,
  keys.map((k) => ({
    key: k,
    e1: e1[k] || missingString,
    e2: e2[k] || missingString,
    diff: getDiff(e1[k], e2[k])
  }))
)
console.log(table)
