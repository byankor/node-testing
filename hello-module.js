const hi = (name) => {
  return `Hi, ${name}`
}

class Hello {
  static hi() {
    return hi('Han')
  }
}

module.exports = hi
module.exports.Hello = Hello
