/**
 * @jest-environment: node
 */

const { N } = require('./nuvei')

describe('test nuvei', () => {

  const n = new N("840f18ff-2b6f-4cfc-a617-2c982ca8f783")

  it('should add doc', async () => {
    const res = await n.addDoc()
    // console.log(res.status, res.data)
    expect(res.status).toBe(204)
  })

  it('should see at least one doc', async () => {
    const res = await n.listDoc()
    // console.log("OUT", res)
  })
})
