const fs = require('fs')

const properties = [ 'address1', 'address2', 'address3' ]

function createFile() {
  let data = [
    `merchant IP address: 127.0.0.1`,
    `date/timestamp: ${new Date()}`,
    `username/email: Han Solo`,
    `click to accept for terms & conditions: Yes`,
    `click to accept for pricing: Yes`,
    `properties:`
  ]

  if (properties.length > 0) {
    data.push(...properties.map(p => `   ${p}`))
  } else {
    data.push('None')
  }

  data.push("\nterms and conditions:") // add a bit of space for readability
  data.push("LONG TEXT HERE")
  fs.writeFileSync('./out.txt', data.join("\n"))
}

createFile()
