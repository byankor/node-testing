// get values as array
const types = {
  issue: 'issue',
  transfer: 'transfer',
  redeem: 'redeem',
}

const arr = Object.values(types)
console.log(arr)

// destructure while converting
const obj = {
  typs: types,
}

// const { typs: Object.values(t) } = obj // error
