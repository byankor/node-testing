class Prop {
  constructor(value) {
    this.value = value;
  }
}
const prop = value => new Prop(value);

const initProps = self => {
  const properties = [];
  Object.entries(self).forEach(([ key, property ]) => {
    if (property instanceof Prop) {
      properties.push(key);
      self[key] = property.value;
    }
  });
  return properties;
};

class P {
  name = prop('Han');
  job = prop('Pilot');
  age = 30

  constructor() {
    this._props = initProps(this);
  }

  get props() {
    return this._props.reduce((properties, key) => (properties[key] = this[key], properties), {});
  }
}

let p = new P()
p.job = 'retired'
console.log(p.props)
