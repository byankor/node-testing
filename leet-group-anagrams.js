const str = ["eat","tea","tan","ate","nat","bat"]

/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function(strs) {
  let out = {}
  for (s of strs) {
    let k = s.split('').sort().join('')
    out[k] ? out[k].push(s) : out[k] = [s]
  }

  return Object.values(out)
};

console.clear()
console.log('out', groupAnagrams(str))
