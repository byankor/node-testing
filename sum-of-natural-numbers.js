function sum(inp) {
  if (inp <= 1)
    return inp

  return inp + sum(inp - 1)
}

console.log(sum(10))
