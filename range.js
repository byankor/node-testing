function ordinalize(n) {
  return n + ([,'st','nd','rd'][n/10%10^1&&n%10]||'th')
}

function ord2(n) {
  return n + (['st', 'nd', 'rd'][((((n + 90) % 100) - 10) % 10) - 1] || 'th')
}

for (var i = 1; i <= 31; i++) {
  console.log(i, "->", ordinalize(i))
  // console.log(i, "->", ord2(i))
}
