// wrong
var str = "OUT"
str =+ "STANDING"
console.log("1", str) // Nan

// right
var str2 = "OUT"
str2 = "STANDING" + str2
console.log("2", str2)

// also right
var str3 = "OUT"
str3 = `STANDING${str3}`
console.log("3", str3)
