const N = 2
const bigList = [
  { status: 'processed', name: 'Malcolm Reynolds' },
  { status: 'not_processed', name: 'Simon Tam' },
  { status: 'not_processed', name: 'River Tam' },
  { status: 'processed', name: 'Zoe Washburne' },
  { status: 'pending', name: 'Inara Serra' },
  { status: 'processed', name: 'Zoe Washburne' },
  { status: 'processed', name: 'Zoe Washburne' },
  { status: 'processed', name: 'Zoe Washburne' },
  { status: 'processed', name: 'Zoe Washburne' },
  { status: 'pending', name: 'Inara Serra' },
  { status: 'pending', name: 'Inara Serra' },
  { status: 'pending', name: 'Inara Serra' },
  { status: 'pending', name: 'Inara Serra' },
]

function byFilter(crew) {
  const truncatedCrew = crew.filter((member) => {
    if (member.status !== 'not_processed') {
      return member
    }
  })

  return truncatedCrew.slice(0, N)
}

function byReduce(crew) {
  crew.reduce((acc, curr) => {
    return curr.status !== 'not_processed'
  })

  return crew
}

function byEach(crew) {
  let list = []
  for(member of crew) {
    if (member.status !== 'not_processed' && list.length < N) {
      list.push(member)
    }
  }
  return list
}

console.log(byEach(bigList))
