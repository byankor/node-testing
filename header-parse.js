const event = {
  headers: {
    'Accept-Encoding': 'identity',
    'Content-Type': 'application/x-www-form-urlencoded',
    Host: 'api.dev.payrent.com',
    Referer: 'https://www.wepay.com',
    'User-Agent': 'WePay API Callback Agent',
    'X-Amzn-Trace-Id': 'Root=1-5fecf2b5-36df4aa5465d1c763564ef64',
    'X-Forwarded-For': '104.154.48.40',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https'
  }
}

const headers = Object.keys(event.headers);
console.log(headers)
for (const key of headers) {
  console.log(key)
  event.headers[key.toLowerCase()] = event.headers[key];
}

console.log("EVENT", event)

