const p1 = new Promise((resolve, reject) => {
  try {
    console.log(1)
    resolve(1)
  } catch(err) {
    reject(err)
  }
})

const p2 = new Promise((resolve, reject) => {
  try {
    console.log(2)
    reject(2)
  } catch(err) {
    reject(err)
  }
})

async function main() {
  await p1.then(
    async () => {
      await p2
    }
  ).catch(err => console.error(err))
}

console.clear()
main()
